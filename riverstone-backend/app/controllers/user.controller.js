const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
let userRoleId = "";
Role.find({ name: "user" }, (err, res) => {
  userRoleId = res[0].id;
})
exports.deactivateUser = (req, res) => {
  console.log(req.body)
  User.findOneAndUpdate({_id: req.body.userId}, { isActive: req.body.status }, (err, resp) => {
    if (err) {
      res.status(403).send({ message: "User not found" })
    }
    res.status(200).send({ body: `User ${req.body.status === true ? "Activated" : "Deactivated"} Successfully` })
  })
}

exports.getUserCount = async (req, res) => {
  let { inactive, active } = 0;
  await User.find({ roles: userRoleId, isActive: true }).count((err, result) => {
    active = result;
  });
  await User.find({ roles: userRoleId, isActive: false }).count((err, result1) => {
    inactive = result1;
  });
  await res.status(200).send({ active, inactive, total: active + inactive })
}

exports.listUsers = async (req,res) => {
  User.find({ roles: userRoleId },{ password: 0, roles: 0 }, (err, result) => {
    if(err){
      res.status(500)
    }
    res.send(result)
  })
}

exports.searchUser = async (req, res) => {
  User.find(
    {
      roles: userRoleId, $or: [
        { "username": { "$in": new RegExp(req.body.query, 'i') }},
        { "email": { "$in": new RegExp(req.body.query, 'i') } }
      ],
      "isActive": req.body.status
    }, (err, result) => {
      console.log("err :", err, "res :", result)
      res.send(result)
    }
  )
}