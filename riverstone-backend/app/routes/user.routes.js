const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");
const emailcontroller = require("../controllers/mail.controller")
module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/api/useractivation",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.deactivateUser
  );

  app.get(
    "/api/usersnapshot",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getUserCount
  );

  app.post(
    "/api/send-email",
    [authJwt.verifyToken],
    emailcontroller.mailer
  );

  app.post(
    "/api/search-user",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.searchUser
  );

  app.get(
    "/api/list-users",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.listUsers
  );
};
