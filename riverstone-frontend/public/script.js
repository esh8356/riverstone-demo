$(document).ready(function() {
    var text_max = 250;
    $('.remaining').html(text_max + ' Characters Left');

    $('#textarea').keyup(function() {
        var text_length = $('#textarea').val().length;
        var text_remaining = text_max - text_length;

        $('.remaining').html(text_remaining + ' Characters Left');
    });
});
