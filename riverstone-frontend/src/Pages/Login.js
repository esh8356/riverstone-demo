import React from 'react'
import axios from 'axios'
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        }
    }

    componentDidMount(){
        if(localStorage.getItem('auth_token')){
            window.location.href = "/"
        }
    }

    checkLogin = () => {
        let url = "http://localhost:8080/api/auth/signin"

        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            data: { username: this.state.username, password: this.state.password },
            url
        }
        var _self = this;
        axios(options)
            .then(async(response) => {
                await localStorage.setItem('auth_token', response.data.accessToken)
                await localStorage.setItem('role', response.data.roles)
                await localStorage.setItem('name', response.data.username.split(" ")[0])
                _self.props.history.push('/')
            })
            .catch(err => {
                alert("Username or password incorrect")
            })
    }

    render() {
        return (
            <div>
                <section id="login-module">
                    <div class="container">
                        <div class="login-card">
                            <div class="brand-logo">Riverstone</div>
                            <div class="login-title">Welcome back!</div>
                            <ul class="login-form">

                                <li>
                                    <label>Email</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail icon-dual"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                            </span>
                                        </div>
                                        <input type="email" id="email" placeholder="hello@eimsolutions.com" onChange={(e) => this.setState({ username: e.target.value })} />
                                    </div>
                                </li>

                                <li>
                                    <label>Password</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock icon-dual"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                            </span>
                                        </div>
                                        <input type="password" id="password" placeholder="....." onChange={(e) => this.setState({ password: e.target.value })} />
                                    </div>
                                </li>

                                <li><button class="login-btn" disabled={!this.state.username || !this.state.password} style={!this.state.username || !this.state.password ? { cursor: "not-allowed" } : {}} onClick={() => this.checkLogin()}>Sign In</button></li>
                                <li>
                                    <small>Don't have an account? <a href="/signup">Sign up now</a></small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default Login