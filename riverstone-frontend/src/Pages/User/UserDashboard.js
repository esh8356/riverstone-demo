import React, { useImperativeHandle } from 'react'
import Dashboard from '../../images/a-dashboard.svg'
import Snap1 from "../../images/snap-g.png"
import Snap2 from "../../images/snap-g2.png"
import Snap3 from "../../images/snap-g3.png"
import axios from "axios"
import { throws } from 'assert';

class UserDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  
            email: "",
            subject: "",
            text: ""
        };
    }

    logout = () => {
        localStorage.clear();
        window.location.reload();
    }

    sendEmail = () => {
        let url = "http://localhost:8080/api/send-email"
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json', 'x-access-token': localStorage.getItem('auth_token') },
            data: { email: this.state.email, subject: this.state.subject, text: this.state.text },
            url
        }
        var _self = this;
        axios(options)
            .then(async(response) => {
                alert("Mail Sent Successfully")
                window.location.reload();
            })
            .catch(err => {
                alert("Username or password incorrect")
            })
    }

    render() {
        let isValid = !this.state.email || !this.state.subject ||!this.state.text ? false : true

        return (
            <div>
                <section id="tob-nav">
                    <div class="col-md-4 pad0">
                        <a href="" class="brand-logo">Riverstone</a>
                    </div>
                    <div class="col-md-8">
                        <div class="admin-block">
                            <div class="a-avatar dropdown">
                                <a class="drop-down dropdown-toggle " id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src={""} alt="" />
                                    <span class="a-avatar-name">{localStorage.getItem('name') ? localStorage.getItem('name') : "User"}</span>
                                    <span class="a-role">User</span>
                                </a>
                                <ul class="list-unstyled dropdown-menu common-dropdown" aria-labelledby="dropdownMenu">
                                    <li onClick={() => this.logout()}>Logout</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="side-nav">
                    <ul class="list-unstyled side-nav-links">
                        <li><a href=""><img src={Dashboard} />Dashboard</a></li>
                        {/* <li><a href=""><img src="images/resource.svg" />Users</a></li> */}
                    </ul>
                </section>
                <section id="login-module">
                    <div class="container">
                        <div class="login-card">
                            <div class="brand-logo">Compose Email</div>
                            <ul class="login-form">

                                <li>
                                    <label>To</label>
                                    <div class="input-group input-group-merge">
                                        <input type="email" id="email" placeholder="hello@eimsolutions.com" onChange={(e) => this.setState({ email: e.target.value })} />
                                    </div>
                                </li>

                                <li>
                                    <label>subject</label>
                                    <div class="input-group input-group-merge">
                                        <input type="text" id="password" placeholder="....." onChange={(e) => this.setState({ subject: e.target.value })} />
                                    </div>
                                </li>

                                <li>
                                    <label>Body</label>
                                    <div class="input-group input-group-merge">
                                        <input type="textarea" id="password" placeholder="....." onChange={(e) => this.setState({ text: e.target.value })} />
                                    </div>
                                </li>

                                <li><button class="login-btn" disabled={!isValid} style={isValid ? {} : {cursor: "not-allowed"}} onClick = {() => this.sendEmail()} >Send Email</button></li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default UserDashboard