import React, { useImperativeHandle } from 'react'
import Dashboard from '../../images/a-dashboard.svg'
import Snap1 from "../../images/snap-g.png"
import Snap2 from "../../images/snap-g2.png"
import Snap3 from "../../images/snap-g3.png"
import axios from "axios"
const debounce = (fn, delay) => {
    let timer = null;
    return function (...args) {
        const context = this;
        timer && clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(context, args);
        }, delay);
    };
}
class AdminDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userSnapshot: {},
            users: [],
            tableHead: {
                Name: 'username',
                Mobile: 'mobile',
                Email: 'email',
                Status: 'isActive',
                Action: 'action'
            },
            query: "",
            filterType: true
        };
        this.fetchUsers = debounce(this.fetchUsers, 200);
    }

    fetchUsers = (query) => {
        let url = "http://localhost:8080/api/search-user"

        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json', 'x-access-token': localStorage.getItem('auth_token') },
            data: { query: query, status: this.state.filterType },
            url
        }
        var _self = this;
        axios(options)
            .then(async (response) => {
                _self.setState({ users: response.data })
            })
            .catch(err => {
                alert("Something went wrong try again later")
            })
    }

    getUsers = () => {
        this.getUserSnapshot();
        let url = "http://localhost:8080/api/list-users"

        const options = {
            method: 'GET',
            headers: { 'content-type': 'application/json', 'x-access-token': localStorage.getItem('auth_token') },
            url
        }
        var _self = this;
        axios(options)
            .then(async (response) => {
                _self.setState({ users: response.data })
            })
            .catch(err => {
                alert("Something went wrong try again later")
            })
    }

    getUserSnapshot = () => {
        let url = "http://localhost:8080/api/usersnapshot"

        const options = {
            method: 'GET',
            headers: { 'content-type': 'application/json', 'x-access-token': localStorage.getItem('auth_token') },
            url
        }
        var _self = this;
        axios(options)
            .then(async (response) => {
                _self.setState({ userSnapshot: response.data })
            })
            .catch(err => {
                alert("Something went wrong try again later")
            })
    }

    logout = () => {
        localStorage.clear();
        window.location.reload();
    }

    componentDidMount() {
        this.getUsers();
    }

    changeUserStatus = (key, value) => {
        let url = "http://localhost:8080/api/useractivation"
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json', 'x-access-token': localStorage.getItem('auth_token') },
            data: { userId: key._id, status: value },
            url
        }
        var _self = this;
        axios(options)
            .then(async (response) => {
                _self.setState({ userSnapshot: response.data }, () => this.getUsers())
            })
            .catch(err => {
                alert("Something went wrong try again later")
            })
    }

    onSearchChange(ev) {
        const query = ev.target.value;
        this.setState({ query });
        this.fetchUsers(query);
    }

    render() {
        let { userSnapshot, users } = this.state;
        return (
            <div>
                <section id="tob-nav">
                    <div class="col-md-4 pad0">
                        <a href="" class="brand-logo">Riverstone</a>
                    </div>
                    <div class="col-md-8">
                        <div class="admin-block">
                            <div class="a-avatar dropdown">
                                <a class="drop-down dropdown-toggle " id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src={""} alt="" />
                                    <span class="a-avatar-name"></span>
                                    <span class="a-role">Admin</span>
                                </a>
                                <ul class="list-unstyled dropdown-menu common-dropdown" aria-labelledby="dropdownMenu">
                                    <li onClick={() => this.logout()}>Logout</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="side-nav">
                    <ul class="list-unstyled side-nav-links">
                        <li><a href=""><img src={Dashboard} />Dashboard</a></li>
                        {/* <li><a href=""><img src="images/resource.svg" />Users</a></li> */}
                    </ul>
                </section>

                <section id="content-wrapper">

                    <div class="row">
                        <div class="col-sm-3 col-6">
                            <div class="common-wcard resource-snap">
                                <div class="rsnap-lft">
                                    <span class="rsnap-title">Total Users</span>
                                    <span class="rsnap-count">{userSnapshot && userSnapshot.total ? userSnapshot.total : 0}</span>
                                </div>
                                <div class="rsnap-rgt">
                                    <div class="rsnap-canvas"><img src={Snap1} alt="" /></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-6">
                            <div class="common-wcard resource-snap">
                                <div class="rsnap-lft">
                                    <span class="rsnap-title">Active</span>
                                    <span class="rsnap-count">{userSnapshot && userSnapshot.active ? userSnapshot.active : 0}</span>
                                </div>
                                <div class="rsnap-rgt">
                                    <div class="rsnap-canvas"><img src={Snap2} alt="" /></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-sm-3 col-6">
                            <div class="common-wcard resource-snap">
                                <div class="rsnap-lft">
                                    <span class="rsnap-title">In Active</span>
                                    <span class="rsnap-count">{userSnapshot && userSnapshot.inactive ? userSnapshot.inactive : 0}</span>
                                </div>
                                <div class="rsnap-rgt">
                                    <div class="rsnap-canvas"><img src={Snap3} alt="" /></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>


                    <div class="common-wcard">
                        <ul class="list-unstyled common-form-fields">
                            <li>
                                <input type="text" name="" placeholder="Search By Email/Name" value={this.state.query} onChange={this.onSearchChange.bind(this)} />
                            </li>
                            <li>
                                <select onChange={(e) => this.setState({ filterType: e.target.value === 'Active' ? true : false }, () => this.fetchUsers())}>
                                    <option>Active</option>
                                    <option>Deactive</option>
                                </select>
                            </li>
                        </ul>
                        <table class="table-responsive common-table common-form-fields" width="100%">
                            <tr>
                                {Object.keys(this.state.tableHead).map(head => (
                                    <th>{head}</th>
                                ))}
                            </tr>
                            <tbody>
                                {users && users.length != 0 ? users.map((i, key) => (
                                    <tr>
                                        {Object.values(this.state.tableHead).map(h =>
                                            h === 'isActive' ? i[h] ? <td>Active</td> : <td>Deactivated</td> :
                                                h === 'action' ? <td class="dropdown">
                                                    <a class="drop-down dropdown-toggle" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                                    <ul class="list-unstyled dropdown-menu" aria-labelledby="dropdownMenu" x-placement="bottom-start">
                                                        {i['isActive'] ? <li onClick={() => this.changeUserStatus(i, false)}>Deactivate User</li> : <li onClick={() => this.changeUserStatus(i, true)}>Activate User</li>}
                                                    </ul>
                                                </td> :
                                                    (<td>{i[h]}</td>))}
                                    </tr>
                                )) : null}
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        )
    }
}

export default AdminDashboard