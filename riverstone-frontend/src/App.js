import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import Login from './Pages/Login.js'
import Signup from './Pages/SignUp'
import AdminDashboard from './Pages/Admin/AdminDashboard'
import UserDashboard from './Pages/User/UserDashboard'
class App extends React.Component {
  render() {
    let isLoggedIn = localStorage.getItem('auth_token');
    let role = localStorage.getItem('role');
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path='/login' component={Login} />
            <Route exact path="/signup" component={Signup} />
            <Route path='/' exact component={(props) => <ProtectedRoutes isLoggedIn={isLoggedIn} role={role} />} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }

}
const ProtectedRoutes = ({ isLoggedIn = false, role = "user" }) => {
  if (isLoggedIn) {
    if (role === "ROLE_ADMIN") {
      return (
        <Switch>
          <Route path="/" component={AdminDashboard} />
        </Switch>
      )
    } else {
      return (
        <Switch>
          <Route path="/" component={UserDashboard} />
        </Switch>
      )
    }

  }
  return <Redirect to={'/login'} />
}
export default App;
